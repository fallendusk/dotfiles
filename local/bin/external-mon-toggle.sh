#!/bin/sh

# Get out of town if something errors
set -e

# Check for Dock monitor
CARD_PATH="/sys/class/drm/card0"
for OUTPUT in $(cd "$CARD_PATH" && echo card0-DP*); do
	OUT_STATUS=$(<"$CARD_PATH"/"$OUTPUT"/status)
	if [[ $OUT_STATUS == connected ]]
	then
		/usr/bin/xrandr --output DP2-1 --auto --above eDP1
		/usr/bin/notify-send --urgency=low -t 5000 "Graphics Update" "HDMI display connected"
		exit 0
	fi
done

# Check for HDMI port display
for OUTPUT in $(cd "$CARD_PATH" && echo card0-HDMI*); do
	OUT_STATUS=$(<"$CARD_PATH"/"$OUTPUT"/status)
	if [[ $OUT_STATUS == connected ]]
	then
		/usr/bin/xrandr --output HDMI2 --auto --above eDP1
		/usr/bin/notify-send --urgency=low -t 5000 "Graphics Update" "HDMI display connected"
		exit 0
	else
    		/usr/bin/xrandr --output DP2-1 --off
		/usr/bin/xrandr --output HDMI2 --off
    		/usr/bin/notify-send --urgency=low -t 5000 "Graphics Update" "External monitor disconnected"    
    		exit 0
	fi
done
