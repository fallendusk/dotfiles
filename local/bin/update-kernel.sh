#!/bin/bash

# Trap INT so we can exit gracefully
trap ctrl_c INT
ctrl_c() {
	echo "** Ctrl-C pressed, exiting... **"
	exit
}

eselect kernel list
echo "Enter target kernel number followed by [ENTER]:"
read ESELECT_KERNEL_SET
eselect kernel set $ESELECT_KERNEL_SET
CURRENT_KERNEL=$(uname -r)
NEW_KERNEL=$(readlink /usr/src/linux | sed 's/linux-//g;')
echo "Current kernel: $CURRENT_KERNEL"
echo "New kernel to build: $NEW_KERNEL"
eselect kernel show
echo "Starting update in 5 seconds. Ctrl-C to exit"
echo -n "5 "; sleep 1; echo -n "4 "; sleep 1; echo -n "3 "; sleep 1; echo -n "2 "; sleep 1; echo -n "1 "; sleep 1


#echo "Copying current kernel config."
#cp -v /etc/kernels/kernel-config-x86_64-$CURRENT_KERNEL /etc/kernels/kernel-config-x86_64-$NEW_KERNEL

cd /usr/src/linux
echo "Running mrproper"
make mrproper

echo "Copying running kernel config into kernel source"
zcat /proc/config.gz > /usr/src/linux/.config
cp -v .config /etc/kernels/config-$CURRENT_KERNEL
#make menuconfig
make olddefconfig

echo "Building kernel"
make -j3
make bzImage

echo "Installing modules"
make modules_install

#echo "Running genkernel"
#genkernel kernel

echo "Rebuilding modules for new kernel"
emerge --usepkg=n --ask=n @module-rebuild

echo "Mounting ESP"
mount /efi

echo "Installing kernel"
cp -v arch/x86/boot/bzImage /boot/kernel-genkernel-x86_64-$NEW_KERNEL

echo "Generating initramfs with dracut"
dracut --hostonly -f /boot/initramfs-genkernel-x86_64-$NEW_KERNEL $NEW_KERNEL

echo "Copying new kernel and initramfs to ESP"
cp -v /boot/kernel-genkernel-x86_64-$NEW_KERNEL /efi/EFI/gentoo/linux.efi
cp -v /boot/initramfs-genkernel-x86_64-$NEW_KERNEL /efi/EFI/gentoo/initramfs.img
cp -v /boot/early_ucode.cpio /efi/EFI/gentoo/early_ucode.cpio

#echo "Updating grub emergency boot"
#grub-mkconfig -o /boot/grub/grub.cfg

umount /efi
echo "All done!"
