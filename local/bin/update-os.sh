#!/bin/bash

SNAPSHOT_NAME="emerge_$(date '+%F-%R:%S')"
SNAPSHOTS_KEEP="2w"

echo "Creating ZFS snapshot..."
sudo zfs snapshot -r rpool/ROOT/gentoo@$SNAPSHOT_NAME

sudo emerge --sync
sudo emerge -auDN @world

echo "Pruning emerge snapshots older than $SNAPSHOTS_KEEP"
sudo ~greg/.local/bin/zfs-prune-snapshots -p 'emerge' $SNAPSHOTS_KEEP rpool

